#include "FastLED.h"

FASTLED_USING_NAMESPACE

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    13
//#define CLK_PIN   4
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    30
CRGB leds[NUM_LEDS];

#define BRIGHTNESS          96
#define FRAMES_PER_SECOND  120

int sensorPin1 = A1; // select the input pin for LDR
int sensorPin2 = A0; // select the input pin for LDR
int sensorPin3 = A2; // select the input pin for LDR
int sensorValue1 = 0; // variable to store the value coming from the sensor
int sensorValue2 = 0; // variable to store the value coming from the sensor
int sensorValue3 = 0; // variable to store the value coming from the sensor
int paired = 0;
int s1val = 1;
int H = 0;
int S = 0;
int V = 0;
void setup ()
{
  Serial.begin (115200);
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  //FastLED.addLeds<LED_TYPE,DATA_PIN,CLK_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);
  Serial.begin(9600);
  for ( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB( 0, 255, 0);
  }

}

uint8_t gHue = 0; // rotating "base color" used by many of the patterns


void loop ()
{
 // sensorValue1 = analogRead(sensorPin1); // read the value from the sensor
  sensorValue2 = analogRead(sensorPin2); // read the value from the sensor
  sensorValue3 = analogRead(sensorPin3); // read the value from the sensor
  // Serial.print("Sensor 1 :"); Serial.println (sensorValue1);
  // Serial.print("Sensor 2 :"); Serial.println (sensorValue2);
  // Serial.print("Sensor 3 :"); Serial.println (sensorValue3);

  FastLED.show();
  FastLED.delay(1000 / FRAMES_PER_SECOND);



  if (sensorValue1 > 100) {
    paired = 1;
   for ( int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CHSV( 255, 200, 255);
    }

  }

  if (sensorValue2 > 100) {
    paired = 2;
       for ( int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CHSV( 150, 205, 255);
    }
  }
  if (sensorValue3 > 100) {
    paired = 3;
       for ( int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CHSV( 75, 205, 255);
    }
    
  }

  if (paired == 1) {

    if (s1val > 5000) {
      H = s1val - 5000;
    }
    else  if (s1val > 4000) {
      S = s1val - 4000;
    }
    else  if (s1val > 3000) {
      V = s1val - 3000;
    }
    else {
      H = 150;
      S = 150;
      V = 150;
    }
    for ( int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CHSV( H, S, V);
    }
  }
  Serial.print("S");
  Serial.println(paired);
}




