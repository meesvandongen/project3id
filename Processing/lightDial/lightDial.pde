import hypermedia.net.*;

PImage drivingMode;
PImage drivingModePointer;
int i=0;

//import udp library
UDP udpRX, udpTX;

//variables for udp
String ip="localhost";
int portRX=51000;
int portTX=50000;
int rx_buffer_size=100;
String rx_string="Waiting for Data";
byte[] rx_byte=new byte[rx_buffer_size];
int rx_byte_count=0;

void setup() {
  fullScreen();
  // Images must be in the "data" directory to load correctly
  drivingMode = loadImage("drivingMode.png");
  drivingModePointer = loadImage("drivingModePointer.png");
  imageMode(CENTER);

  //create new object for receiving 
  udpRX=new UDP(this, portRX, ip);
  udpRX.log(true);
  udpRX.listen(true);
}

void draw() {
  image(drivingMode, width/2, height/2, width, height);
  translate(.5f*width, .95f*height);
  rotate(radians(i++));
  image(drivingModePointer, 0, 0);
}
//udp receive handling
void receive(byte[] data, String ip, int portRX) {

  String value=new String(data);
  println(value);
  rx_byte_count+=data.length;

  //clear receive byte array
  for (int j=0; j<rx_byte.length; j++)
  {
    rx_byte[j]=0;
  }

  //copy received bytes to rx_byte array
  arrayCopy(data, rx_byte);

  //copy bytes to a string and trim string to remove nulls
  rx_string=new String(rx_byte);
  rx_string=trim(rx_string);

  //is called to update the received string
  redraw();
}
