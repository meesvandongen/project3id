import hypermedia.net.*;

PImage drivingMode0;
PImage drivingMode1;
PImage drivingMode2;
PImage drivingMode3;

PImage drivingMode;


PImage drivingModePointer;
int i=0;
float speed=0;

//import udp library
UDP udpRX, udpTX;

//variables for udp
String ip="localhost";
int portRX=51000;
int rx_buffer_size=100;
String rx_string="Waiting for Data";
String pair;
byte[] rx_byte=new byte[rx_buffer_size];
int rx_byte_count=0;

boolean isStarted = false;

void setup() {
  //size(1280, 720);
  fullScreen();
  // Images must be in the "data" directory to load correctly
  drivingMode0 = loadImage("drivingMode.png");
  drivingMode1 = loadImage("drivingMode2.png");
  drivingMode2 = loadImage("drivingMode3.png");
  drivingMode3 = loadImage("drivingMode4.png");

  drivingModePointer = loadImage("drivingModePointer.png");
  imageMode(CENTER);

  //create new object for receiving 
  udpRX=new UDP(this, portRX, ip);
  udpRX.log(true);
  udpRX.listen(true);
}

void draw() {

  //if (keyPressed) {
  //  if (key == 'a' || key == 'A') {
  //    decreaseSpeed();
  //  }
  //  if (key == 's' || key == 'S') {
  //    increaseSpeed();
  //  }
  //} else {
  //  //onSelectStop();
  //}

  checkSelected();

  if (!isStarted) {
    println("stopped");
    onSelectStop();
  }

  image(drivingMode, width/2, height/2, width, height);
  translate(.5f*width, .95f*height);
  rotate(radians(speed-90));
  image(drivingModePointer, 0, 0);
  
}
//udp receive handling
void receive(byte[] data, String ip, int portRX) {

  String value=new String(data);
  println(value);
  rx_byte_count+=data.length;
  //clear receive byte array
  for (int j=0; j<rx_byte.length; j++)
  {
    rx_byte[j]=0;
  }

  //copy received bytes to rx_byte array
  arrayCopy(data, rx_byte);

  //copy bytes to a string and trim string to remove nulls
  rx_string=new String(rx_byte);
  rx_string=trim(rx_string);
  String[] list = split(rx_string, ',');
  isStarted = list[6].indexOf("true")>-1? true :false;
  if (isStarted) {
    changeSpeed(parseFloat(list[5]));
  }
  //is called to update the received string
  redraw();
}



void changeSpeed(float val) {
  speed+=(val/50.0);
  if (speed > 180) speed = 180;
  if (speed < 0) speed = 0;
}

void checkSelected() {
  if (speed < 40 ) {
    drivingMode = drivingMode0;
  } else if (speed < 90) {
    drivingMode = drivingMode1;
  } else if (speed < 140) {
    drivingMode = drivingMode2;
  } else {
    drivingMode = drivingMode3;
  }
}

void onSelectStop () {
  println("onSelectStop");
  if (speed < 40 ) {
    speed = 20;
  } else if (speed < 90) {
    speed = 65;
  } else if (speed < 140) {
    speed = 115;
  } else {
    speed = 160;
  }
}