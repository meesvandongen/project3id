//import ui library for buttons
import controlP5.*;
ControlP5 cp5;
import processing.serial.*;
Serial myPort;  // The serial port
Serial otherPort;
int buttonSwitch = 10;

//import udp library
import hypermedia.net.*;
UDP udpRX, udpTX;

//variables for udp
String ip="localhost";
int portRX=50000;
int portTX=51000;
int rx_buffer_size=100;
String rx_string="Waiting for Data";
byte[] rx_byte=new byte[rx_buffer_size];
int rx_byte_count=0;

//set strings for sending on udp
String s0="Zero";
String s1="One";
String s2="Two";

int[] xAccel  = new int[100];
int[] yAccel  = new int[100];
int[] zAccel  = new int[100];
int accelPointer = 0;


int[] yaw = new int[100];
int[] pitch = new int[100];
int[] roll = new int[100];
int rotationPointer = 0;

int iwidth;

boolean isGoingX;

boolean buttonIsPressed;
int light = 0;
int speedDialPort = 51000;
int lightPort = 51001;
int musicPort = 51002;


// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void setup() {

  //set window dimensions
  size(700, 500);

  //disables drawing outlines
  noStroke();

  //create new cp5 object for this user
  cp5=new ControlP5(this);

  ////create 3 buttons 
  //for (int i=0; i<3; i++) {
  //  cp5.addBang("Send"+i)
  //    .setPosition(400+i*80, 200)
  //    .setSize(60, 40)
  //    .setColorForeground(140)
  //    .setId(i)
  //    ;
  //}//end of for loop 

  //create new object for transmitting
  udpTX=new UDP(this);
  udpTX.log(true);
  udpTX.setBuffer(5);
  udpTX.loopback(true);

  //create new object for receiving 
  udpRX=new UDP(this, portRX, ip);
  udpRX.log(true);
  udpRX.listen(true);


  //confirm if tx is multicast and loopback is disabled
  println("Is TX mulitcast: "+udpTX.isMulticast());
  println("Has TX joined multicast: "+udpTX.isJoined());
  println("Is loopback enabled on TX: "+udpTX.isLoopback());

  //confirm if rx is multicast 
  println("Is RX mulitcast: "+udpRX.isMulticast());
  println("Has RX joined multicast: "+udpRX.isJoined());

  initSerial();
  iwidth = (width/yaw.length);

  //stop looping setup  
  noLoop();
}//end of setup()

void initSerial() {
  // List all the available serial ports
  printArray(Serial.list());
  // Open the port you are using at the rate you want:
  myPort = new Serial(this, Serial.list()[0], 19200);
  otherPort = new Serial(this, Serial.list()[1], 9600);
}

// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================

void draw() {
  //set background color, also clears the screen
  background(0);
  readSerialString();
  readSerialSecond();

  fill(255);
  text(str(light), 300, 300);


  otherPort.write(str(random(255)+5000));

  //fill(255,0,0);
  //rect(yawPointer*iwidth, 0, iwidth,  700);

  //fill(255);
  //for(int i = 0; i < yaw.length-1; i+=1) {
  //  rect(i*iwidth, 100, iwidth,  yaw[i]);
  //}

  fill(255, 0, 0);
  rect(accelPointer*iwidth, 0, iwidth, 700);

  fill(255);
  for (int i = 0; i < xAccel.length-1; i+=1) {
    rect(i*iwidth, 100, iwidth, yAccel[i]/100);
  }
  for (int i = 0; i < yAccel.length-1; i+=1) {
    rect(i*iwidth, 200, iwidth, xAccel[i]/100);
  }
  for (int i = 0; i < zAccel.length-1; i+=1) {
    rect(i*iwidth, 300, iwidth, zAccel[i]/100);
  }
  if (buttonIsPressed) {
    fill(0, 255, 0);
  } else {
    fill(255, 0, 0);
  }
  rect(400, 400, 20, 20);

  doInteractions();

  loop();
}

void doInteractions() {
  // if (light == 1) {
  //   udpTX.send(buildString(), "localhost", lightPort);
  // } else 
  if (light ==2) {
    udpTX.send(buildString(), "localhost", speedDialPort);
  } else if (light ==3) {
    udpTX.send(buildString(), "localhost", musicPort);
  }
}

String buildString() {
  String temp = "";
  temp +=xAccel[accelPointer]; // 0
  temp += ",";
  temp +=yAccel[accelPointer]; // 1
  temp += ",";
  temp +=zAccel[accelPointer]; // 2
  temp += ",";
  temp +=yaw[rotationPointer]; // 3
  temp += ",";
  temp +=pitch[rotationPointer]; // 4
  temp += ",";
  temp +=roll[rotationPointer]; // 5
  temp += ",";
  temp+=str(buttonIsPressed); // 6
  temp +=  ",";
  temp += light; // 7
  return temp;
}

public void readSerialSecond() {
  while (otherPort.available() > 0) {
    String inBuffer = otherPort.readString();  
    println(inBuffer);

    if (inBuffer != null) {
      if (inBuffer.indexOf("1") > -1) {
        light = 1;
      } else if (inBuffer.indexOf("2") > -1) {
        light = 2;
      } else if (inBuffer.indexOf("3") > -1) {
        light = 3;
      }
    }
  }
}

public void readSerialString() {
  while (myPort.available() > 0) {
    String inBuffer = myPort.readString();   
    if (inBuffer != null) {
      String[] list = split(inBuffer, '\t');
      for (int i = 0; i < list.length - 1; i++) {
        if (list[i].equals("a") && list.length >= i + 4) {
          setAccel(int(list[++i]), int(list[++i]), int(list[++i]));
        } else if (list[i].equals("y") && list.length >= i + 4) {
          setYRP(int(list[++i]), int(list[++i]), int(list[++i]));
        } else if (list[i].equals("b") && list.length >= i + 2) {
          setButtonPressed(list[++i]);
        }
      }
    }
  }
}

//void detectValidRoll() {
//  if (roll[rotationPointer] > 10 || roll[rotationPointer] < -10) udpTX.send(str(roll[rotationPointer])+","+str(buttonIsPressed), "localhost", portTX);
//}

//void detectValidYaw () {
//  if (yaw[rotationPointer] > 10 || yaw[rotationPointer] < -10) udpTX.send(str(yaw[rotationPointer]), "localhost", portTX);
//}

void setAccel(int a, int b, int c) {
  accelPointer+=1;
  if (accelPointer > xAccel.length -1) accelPointer = 0;
  xAccel[accelPointer] = a;
  yAccel[accelPointer] = b;
  zAccel[accelPointer] = c;
}
void setYRP(int a, int b, int c) {
  rotationPointer += 1;
  if (rotationPointer > yaw.length -1) rotationPointer = 0;
  yaw[rotationPointer] = a;
  roll[rotationPointer] = b;
  pitch[rotationPointer] = c;
}

//event handling 
public void controlEvent(ControlEvent theEvent) {

  if (theEvent.getController().getId()==0) {
    println("String Sent: "+s0);
    udpTX.send(s0, "localhost", portTX);
  }

  if (theEvent.getController().getId()==1) {
    println("String Sent: "+s1);
    udpTX.send(s1, ip, portTX);
  }

  if (theEvent.getController().getId()==2) {
    println("String Sent: "+s2);
    udpTX.send(s2, ip, portTX);
  }
}//end of event


//udp receive handling
void receive(byte[] data, String ip, int portRX) {

  String value=new String(data);
  rx_byte_count+=data.length;

  //clear receive byte array
  for (int j=0; j<rx_byte.length; j++)
  {
    rx_byte[j]=0;
  }

  //copy received bytes to rx_byte array
  arrayCopy(data, rx_byte);

  //copy bytes to a string and trim string to remove nulls
  rx_string=new String(rx_byte);
  rx_string=trim(rx_string);
  String[] list = rx_string.split(";"); 
  if (rx_string.indexOf("l") > -1) {
    otherPort.write(list[1]);
  }
  if (rx_string.indexOf("r") > -1) {
    otherPort.write(list[1]);
  }
  myPort.write("23");
  //is called to update the received string
  redraw();
}

void setButtonPressed(String i) {
  if (i.equals("1")) {
    onPressButton();
  } else {
    onReleaseButton();
  }
}
void onPressButton() {
  buttonIsPressed = true;
}

void onReleaseButton() {
  buttonIsPressed = false;
}