import hypermedia.net.*;
import processing.sound.*;

SoundFile file;

SoundFile[] songs = new SoundFile[3];


//config
int threshold = 20;


//import udp library
UDP udpRX, udpTX;

//variables for udp
String ip="localhost";
int portRX = 51002;
int portTX =50000;
int rx_buffer_size=100;
String rx_string="Waiting for Data";
byte[] rx_byte=new byte[rx_buffer_size];
int rx_byte_count=0;

String[] command;
PImage drivingMode;
int i=0;
int currentSong = 0;
boolean button  = false;

int initialPosition;
int waitForFrame = 0;
int currentFrameCount = 0;

void setup() {
  file = new SoundFile(this, "silence.mp3");
  songs[0] = new SoundFile(this, "./Music/1.mp3");
  songs[1] = new SoundFile(this, "./Music/2.mp3");
  songs[2] = new SoundFile(this, "./Music/3.mp3");

  //fullScreen();
  size(720, 540);
  // Images must be in the "data" directory to load correctly
  drivingMode = loadImage("autoradio.png");
  imageMode(CENTER);

  //create new object for receiving 
  udpRX=new UDP(this, portRX, ip);
  udpRX.log(true);
  udpRX.listen(true);
}

void draw() {
  image(drivingMode, width/2, height/2, width, height);
  translate(.5f*width, .95f*height);
  rotate(radians(i++));
}

//udp receive handling
void receive(byte[] data, String ip, int portRX) {

  String value=new String(data);
  println(value);
  rx_byte_count+=data.length;
  //clear receive byte array
  for (int j=0; j<rx_byte.length; j++)
  {
    rx_byte[j]=0;
  }

  //copy received bytes to rx_byte array
  arrayCopy(data, rx_byte);

  //copy bytes to a string and trim string to remove nulls
  rx_string=new String(rx_byte);
  rx_string=trim(rx_string);
  String[] list = split(rx_string, ',');
  if (!button && list[6].indexOf("true")>-1) { //we have a new button press!
    println("buttonPress");
    initialPosition = parseInt(list[3]);
  }
  button = list[6].indexOf("true")>-1? true :false;
  if (button) {
    doCommands(parseInt(list[3]));
  }
  //is called to update the received string
  redraw();
}

void doCommands(int value) {
  currentFrameCount++;
  if (currentFrameCount > waitForFrame) {
    if (value-initialPosition > threshold) {
      nextSong();
      waitForFrame = currentFrameCount + 20;
    } else if (value-initialPosition < -threshold) {
      previousSong();
      waitForFrame = currentFrameCount + 20;
    }
  }
}

void nextSong() {
  currentSong+=1;
  if (currentSong > songs.length - 1) {
    currentSong = 0;
  }
  playNewFile(songs[currentSong]);
}

void previousSong() {
  currentSong-=1;
  if (currentSong <0) {
    currentSong = songs.length- 1;
  }
  playNewFile(songs[currentSong]);
}

void playNewFile(SoundFile soundFile) {
  file.stop();
  file = soundFile;
  file.play();
}

void mousePressed() {
  if (mouseButton == LEFT) {
    nextSong();
  } else if (mouseButton == RIGHT) {
    previousSong();
  }
}